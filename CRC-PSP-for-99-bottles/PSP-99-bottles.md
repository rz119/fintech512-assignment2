# PSP for 99 Bottles

## Problem description

Write code to pass the tests for 99 bottles

1. Fork this repo.
2. Ensure that you can build the code and run the tests. The repo has a single passing test.
3. Once you can build and run the code, set a two hour timer, and continue with the following steps.
4. Remove the '@Disabled("Remove this line to add this test.")' line from the next test in BottlesTest.Java
5. Write the code to pass the test enabled in step 4.
6. When the code passess all enabled tests, commit your changes. The commit message should name the test that was passed.
7. Return to step 4, until you pass all tests, or two hours have passed.

## Conceptual design

| BottleNumber |    25 lines of code     |
|:------------:|:-----------------------:|
|     next     |     1 line of code      |
|    plural    |     3 lines of code     |
|  container   | around 10 lines of code |
|    action    | around 10 lines of code |
|    range     | 3 lines of code  |

| Bottle | 20 lines of code |
|:------:| :--------------: |
| Verse  | 10 lines of code |
|  Song  | 10 lines of code |
|        |                  |
|        |                  |

| HandleString | 10 lines of code |
|:------------:|:----------------:|
|    plural    | 5 lines of code  |
|  capitalize  | 5 lines of code  |
|              |                  |
|              |                  |

## Planning

|   Total   | 70 mins |
|:---------:| :-----: |
|   next    | 10 mins |
|  Plural   | 10 mins |
| Container | 10 mins |
|  Action   | 10 mins |
|   Verse   | 10 mins |
|   Song    | 10 mins |

## Development

|    Issues    |                                                                                                       |
|:------------:|-------------------------------------------------------------------------------------------------------|
| testVerse2() | At the first glance, it is difficult to find the differnce between "take it down" and "take one down" |
| testVerse0() | Need to handle the capitalization problem                                                             |
|      testACoupleVerses()        | a repitition of one verse, should consider line break                                                 |
|              |                                                                                                       |

| Development Time |         |
|:----------------:|---------|
|       next       | 1 min   |
|      Plural      | 5 mins  |
|    Container     | 5 mins  |
|      Action      | 5 mins  |
|      Verse       | 10 mins |
|       Song       | 1 min   |

| BottleNumber | 20 lines of code |
|:------------:|:----------------:|
|     next     |  2 line of code  |
|    plural    | 4 lines of code  |
|  container   | 4 lines of code  |
|    action    | 9 lines of code  |
|    range     |  1 line of code  |

| Bottle | 7 lines of code |
|:------:|:---------------:|
| Verse  | 6 lines of code |
|  Song  | 1 lines of code |
|        |                 |
|        |                 |

| HandleString | 2 lines of code |
|:------------:|:---------------:|
|    plural    | 1 lines of code |
|  capitalize  | 1 lines of code |

## Testing

|               Defect               | Testing Time |
|:----------------------------------:|-------------|
|    testAnotherVerse() No defect    | 1 min       |
|       testVerse2() No defect       | 1 min       |
|       testVerse1() No defect       | 7 mins      |
|       testVerse0() No defect       | 8 mins      |
|   testACoupleVerses() No defect    | 10 mins     |
|             testAFewVerses() No defect             | 1 min       |
|             testTheWholeSong() No defect             | 1 min       |

## Evaluation

- Total time: 29 mins + 27 mins = 54 mins
- Notable issues:
  - use collectors smartly