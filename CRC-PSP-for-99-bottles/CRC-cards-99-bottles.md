# CRC Cards for 99 Bottles

|                      BottleNumber                      |              |
|:------------------------------------------------------:|:------------:|
|                     Responsbility                      | Collaborator |
|    findNext(Reversely count the number of bottles)     |    Bottle    |
| Plural(Provide plural nouns according to # of bottles) | HandleString |
|   Container (bottles on the wall or no more bottles)   |              |
|   Action (whether to buy or to take the bottle down)   |              |
|     range (reverse the start and end verse number)     |              |

|     Bottle     |              |
| :------------: | :----------: |
| Responsibility | Collaborator |
|     Verse      | BottleNumber |
|      Song      |              |

|                    HandleString                    |              |
|:--------------------------------------------------:|:------------:|
|                   Responsibility                   | Collaborator |
|          plural (find plural of a string)          | BottleNumber |
| capitalize (capitalize the first char of a string) |    Bottle    |