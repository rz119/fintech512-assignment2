public class BottleNumber implements HandleString {
    private int verseNumber;
    public static final int maxVerseNumber = 99;
    public static final int minVerseNumber = 0;

    BottleNumber(int verseNumber){
        this.verseNumber = verseNumber;
    }

    public int next(){
        int next = verseNumber - 1;
        return next >= minVerseNumber ? next : maxVerseNumber;
    }

    @Override
    public String plural(String word){
        if (verseNumber > 1){
            return word + "s";
        }
        return word;
    }

    public String container(){
        if (verseNumber == 0){
            return "no more bottles of beer";
        }
        return verseNumber + " " + plural("bottle") + " of beer";
    }

    public String action(){
        if (verseNumber > 1){
            return "Take one down and pass it around, ";
        }
        else if (verseNumber == 1){
            return "Take it down and pass it around, ";
        }
        else {
            return "Go to the store and buy some more, ";
        }
    }

    @Override
    public String capitalize(String str){
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

}
