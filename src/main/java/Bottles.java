import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Bottles {
	public static final int maxVerseNumber = 99;
	public static final int minVerseNumber = 0;

	public String verse(int verseNumber) {
		BottleNumber bottleNum = new BottleNumber(verseNumber);
		BottleNumber bottleNumNext = new BottleNumber(bottleNum.next());
		return bottleNum.capitalize(bottleNum.container()) + " on the wall, " + bottleNum.container() +
				".\n"  + bottleNum.action() + bottleNumNext.container() + " on the wall.\n";
	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		return range(startVerseNumber,endVerseNumber).mapToObj(this::verse)
				.collect(Collectors.joining("\n"));
	}

	public String song() {
		return verse(maxVerseNumber, minVerseNumber);
	}

	private IntStream range(int from, int to)
	{
		return IntStream.range(to, from+1).map(i -> to - i + from);
	}
}
