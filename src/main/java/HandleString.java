public interface HandleString {
    String plural(String word);
    String capitalize(String str);
}
